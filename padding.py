import glob
from cv2 import cv2
#IMAGE SIZE
IMG_SIZE = 800
#ajouter des bordures pour éviter la déformation
ADDPADDING = 1
#chemin des données à extraires
path = "D:/DataBaseEffa/images/DATA_BASE/sansSanglier/"
#chemin des données à écrire
whereToWrite = "D:/DataBaseEffa/images/DATA_BASE/withPadding/sansSanglier/sansSanglier"

imgs = []
i = 0


def add_padding(img_array):
    ht, wd, ch = img_array.shape
    if wd > ht: #la largeur est plus grande que la hauteur
        numberOfPixelsToAdd = wd - ht
        new_array = cv2.copyMakeBorder(img_array, bottom=numberOfPixelsToAdd, top=0, left=0, right=0, borderType=cv2.BORDER_CONSTANT, value=[255,255,255])
    elif ht > wd: #la hauteur est plus grande que la largeur
        numberOfPixelsToAdd = ht - wd
        new_array = cv2.copyMakeBorder(img_array, bottom=0, top=0, left=numberOfPixelsToAdd, right=0, borderType=cv2.BORDER_CONSTANT, value=[255,255,255])
    elif ht == wd: return img_array
    return new_array


for file in glob.glob(path + "*"):
    imgs.append(file)
print(len(imgs))
for img in imgs:
    img_array = cv2.imread(img, cv2.IMREAD_COLOR)
    if ADDPADDING:
        img_array = add_padding(img_array)
    #img_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    cv2.imwrite(whereToWrite + str(i) + ".jpg", img_array)
    i += 1
    print(i)


