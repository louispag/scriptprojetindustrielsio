import os
import glob
import random
import shutil

DATANUMBER = 7
PATH_BASE = 'D:/DataBaseEffa/images/dataset'
PATH_AVEC = 'D:/DataBaseEffa/images/videos/imagesAvec/final/avec/'
PATH_SANS = 'D:/DataBaseEffa/images/videos/imagesAvec/final/sans/'
#PATH_AVEC = 'D:/DataBaseEffa/images/DATA_BASE/withPadding/avecSanglier/'
#PATH_SANS = 'D:/DataBaseEffa/images/DATA_BASE/withPadding/sansSanglier/'

nbrPicAvec = len([f for f in os.listdir(PATH_AVEC)])
print(nbrPicAvec)
nbrPicSans = len([f for f in os.listdir(PATH_SANS)])
print(nbrPicSans)

nbrPicAvec = 855
nbrPicSans = 985

if os.path.isdir(PATH_BASE + str(DATANUMBER)) is False:
    os.makedirs(PATH_BASE + str(DATANUMBER))
    os.makedirs(PATH_BASE + str(DATANUMBER) + '/train/avecSanglier/')
    os.makedirs(PATH_BASE + str(DATANUMBER) + '/train/sansSanglier/')
    os.makedirs(PATH_BASE + str(DATANUMBER) + '/valid/avecSanglier/')
    os.makedirs(PATH_BASE + str(DATANUMBER) + '/valid/sansSanglier/')
    print('début')
    for i in random.sample(glob.glob(PATH_SANS + '*'), int(0.9 * nbrPicSans)):
        shutil.move(i, PATH_BASE + str(DATANUMBER) + '/train/sansSanglier/')
    print('fin train sans')
    for i in random.sample(glob.glob(PATH_AVEC+ '*'), int(0.9 * nbrPicAvec)):
        shutil.move(i, PATH_BASE + str(DATANUMBER) + '/train/avecSanglier/')
    print('fin train avec')
    for i in random.sample(glob.glob(PATH_SANS+ '*'), int(0.1 * nbrPicSans)):
        shutil.move(i, PATH_BASE + str(DATANUMBER) + '/valid/sansSanglier/')
    print('fin valid sans')
    for i in random.sample(glob.glob(PATH_AVEC+ '*'), int(0.1 * nbrPicAvec)):
        shutil.move(i, PATH_BASE + str(DATANUMBER) + '/valid/avecSanglier/')
    print('fin valid avec')
