import numpy as np
import os
import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras.layers import Dense,GlobalAveragePooling2D,Dropout
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
import multiprocessing 

train_path = 'D:/DataBaseEffa/images/dataset6_resized400/train/'
valid_path = 'D:/DataBaseEffa/images/dataset6_resized400/valid/'
MODEL_PATH = 'D:/DataBaseEffa/MobileNetV2_50epochs2_1ereEtapeEndLayers_/'

train_batches = ImageDataGenerator(
                        featurewise_center=True,
                        channel_shift_range=100.0,
                        zca_whitening=True,
                        brightness_range=[0.85, 1.15],
                        zoom_range=[0.85, 1.0],
                        rotation_range=15,
                        horizontal_flip=True,
                        #fill_mode="wrap",
                        #width_shift_range=0.1,
                        rescale= 1.0/255.0
                        )\
    .flow_from_directory(directory=train_path,
                        target_size=(400, 400),
                        class_mode='binary',
                        shuffle = True,
                        save_format= 'jpg',
                        classes= ["sansSanglier", "avecSanglier"],
                        batch_size=25
                        )

valid_batches = ImageDataGenerator(
                        #preprocessing_function=preprocess_input
                        #horizontal_flip=True,
                        #rotation_range=20
                        rescale= 1.0/255.0
                        )\
    .flow_from_directory(directory=valid_path,
                        target_size=(400, 400),
                        class_mode='binary',
                        save_format= 'jpg',
                        classes= ["sansSanglier", "avecSanglier"]
                        )

checkpoint_path = "D:/DataBaseEffa/MobileNetV2_50epochs2_1ereEtapeEndLayers_100epochs_CP/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)

model_base = keras.models.load_model(MODEL_PATH)
model = keras.Sequential()

for i,layer in enumerate(model_base.layers):
  layer.trainable = True
  print(i,layer.name,layer.output_shape)

for layer in model_base.layers[:-4]:
  layer.trainable = False
  model.add(layer)
print(model.summary())

inputs = keras.Input(shape=(400, 400, 3))
x = model(inputs)
x=Dense(1024,activation='relu')(x)
x=Dense(512,activation='relu')(x) #dense layer 2
outputs = keras.layers.Dense(1, activation='sigmoid')(x)
model = keras.Model(inputs, outputs)

print(model.summary())


#model.layers.mobilenetv2_1.trainable = True
#print(model.summary())
#model.compile(optimizer=Adam(learning_rate=1e-5), loss='binary_crossentropy', metrics=['accuracy'])
model.compile(optimizer=Adam(), loss='binary_crossentropy', metrics=['accuracy'])


model.fit(
    x=train_batches,
    steps_per_epoch=len(train_batches),
    validation_data=valid_batches,
    validation_steps=len(valid_batches),
    epochs=10,
    verbose=1,
    #use_multiprocessing=True
)

model.save('D:/DataBaseEffa/MobileNetV2_50epochs_plus30epochs1e-5_rezoEND')


