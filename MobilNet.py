import numpy as np
import os
import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
from tensorflow.keras.applications import MobileNet
from tensorflow.keras.layers import Dense,GlobalAveragePooling2D,Dropout
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
import multiprocessing 

train_path = 'D:/DataBaseEffa/images/dataset8_400/train/'
valid_path = 'D:/DataBaseEffa/images/dataset8_400/valid/'

train_batches = ImageDataGenerator(
                        preprocessing_function=preprocess_input,
                        featurewise_center=True,
                        featurewise_std_normalization=True,
                        channel_shift_range=70.0,
                        zca_whitening=True,
                        brightness_range=[0.85, 1.00],
                        zoom_range=[0.80, 1.00],
                        rotation_range=15,
                        horizontal_flip=True,
                        fill_mode="wrap",
                        width_shift_range=0.15
                        #rescale= 1.0/255.0
                        )\
    .flow_from_directory(directory=train_path,
                        target_size=(400, 400),
                        class_mode='binary',
                        batch_size=32,
                        shuffle = True,
                        save_format= 'jpg',
                        classes= ["sansSanglier", "avecSanglier"]
                        )

valid_batches = ImageDataGenerator(
                        preprocessing_function=preprocess_input
                        #horizontal_flip=True,
                        #rotation_range=20
                        #rescale= 1.0/255.0
                        )\
    .flow_from_directory(directory=valid_path,
                        target_size=(400, 400),
                        class_mode='binary',
                        save_format= 'jpg',
                        classes= ["sansSanglier", "avecSanglier"]
                        )

base_model=MobileNet(  
                    input_shape=(400,400,3),
                    weights='imagenet',
                    include_top=False
                    )

#ca ne marchait pas avec ça
#inputs = keras.Input(shape=(400, 400, 3))
#x = keras.applications.mobilenet_v2.preprocess_input(inputs)
#x = base_model(x, training = True)
#model = keras.Model(inputs, outputs)

base_model.trainable = False  
x=base_model.output
x = keras.layers.GlobalAveragePooling2D()(x)
x=Dense(1000,activation='relu')(x)
x=Dropout(0.2)(x)
x=Dense(1000,activation='relu')(x)
x=Dropout(0.2)(x)
x=Dense(500,activation='relu')(x)
x=Dropout(0.2)(x)
x=Dense(250,activation='relu')(x)
#x=Dense(128,activation='relu')(x)
#x=Dense(64,activation='relu')(x)
outputs = keras.layers.Dense(1, activation='sigmoid')(x)
model=Model(inputs=base_model.input,outputs=outputs)

#for i,layer in enumerate(model.layers):
#  print(i,layer.name,layer.output_shape)
print(model.summary())

model.compile(optimizer='Adam',loss='binary_crossentropy',metrics=[keras.metrics.BinaryAccuracy()])

# Create a callback that saves the model's weights
checkpoint_path = "D:/DataBaseEffa/MobileNet_1ereEtapeEndLayersDROP_NEW_CP/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)
cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                  save_weights_only=True,
                                                  verbose=0)
es = tf.keras.callbacks.EarlyStopping(monitor='accuracy', mode='max', verbose=0, patience=15, restore_best_weights=True)


model.fit(
    x=train_batches,
    steps_per_epoch=len(train_batches),
    validation_data=valid_batches,
    validation_steps=len(valid_batches),
    epochs=70,
    verbose=1,
    callbacks=[cp_callback,es]
)

model.save('D:/DataBaseEffa/MobileNetV_1ereEtapeEndDROPLayersNEW_')

#2eme partie
base_model.trainable = True
print(model.summary())
model.compile(optimizer=Adam(learning_rate=1e-5), loss='binary_crossentropy', metrics=['accuracy'])
checkpoint_path = "D:/DataBaseEffa/MobileNetDROP_2emeEtapeFT_CP/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)
cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                  save_weights_only=True,
                                                  verbose=0)
es = tf.keras.callbacks.EarlyStopping(monitor='accuracy', mode='max', verbose=0, patience=10, restore_best_weights=True)
model.fit(
    x=train_batches,
    steps_per_epoch=len(train_batches),
    validation_data=valid_batches,
    validation_steps=len(valid_batches),
    epochs=70,
    verbose=1,
    callbacks=[cp_callback,es]
)
model.save('D:/DataBaseEffa/MobileNetV_DROP_2emeEtapeNEW_')