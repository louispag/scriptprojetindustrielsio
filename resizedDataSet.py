from cv2 import cv2
import glob

path = "D:/DataBaseEffa/images/dataset6_resized600/train/sansSanglier/"
pathtowrite = "D:/DataBaseEffa/images/dataset6_resized600/train/sansSanglierr/"
imgs = []
i = 0
IMG_SIZE = 700

for file in glob.glob(path + "*"):
    imgs.append(file)
print(len(imgs))
for img in imgs:
    img_array = cv2.imread(img, cv2.IMREAD_COLOR)
    img_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    cv2.imwrite(pathtowrite + str(i) + ".jpg", img_array)
    i = i + 1

path = "D:/DataBaseEffa/images/dataset6_resized600/train/avecSanglier/"
pathtowrite = "D:/DataBaseEffa/images/dataset6_resized600/train/avecSanglierr/"
imgs = []
i = 0

for file in glob.glob(path + "*"):
    imgs.append(file)
print(len(imgs))
for img in imgs:
    img_array = cv2.imread(img, cv2.IMREAD_COLOR)
    img_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    cv2.imwrite(pathtowrite + str(i) + ".jpg", img_array)
    i = i + 1

path = "D:/DataBaseEffa/images/dataset6_resized600/valid/sansSanglier/"
pathtowrite = "D:/DataBaseEffa/images/dataset6_resized600/valid/sansSanglierr/"
imgs = []
i = 0

for file in glob.glob(path + "*"):
    imgs.append(file)
print(len(imgs))
for img in imgs:
    img_array = cv2.imread(img, cv2.IMREAD_COLOR)
    img_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    cv2.imwrite(pathtowrite + str(i) + ".jpg", img_array)
    i = i + 1

path = "D:/DataBaseEffa/images/dataset6_resized600/valid/avecSanglier/"
pathtowrite = "D:/DataBaseEffa/images/dataset6_resized600/valid/avecSanglierr/"
imgs = []
i = 0

for file in glob.glob(path + "*"):
    imgs.append(file)
print(len(imgs))
for img in imgs:
    img_array = cv2.imread(img, cv2.IMREAD_COLOR)
    img_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    cv2.imwrite(pathtowrite + str(i) + ".jpg", img_array)
    i = i + 1