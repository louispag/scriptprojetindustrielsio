from cv2 import cv2
from tensorflow import keras
import numpy as np
from PIL import Image
from skimage import transform
import glob
import os 

def crop_image(img):
    height, width, ch = img.shape
    if width == height:
        return img
    img = np.array(img)
    offset  = int(abs(height-width)/2)
    if width>height:
        img = img[:,offset:width-offset,:]
    else:
        img = img[offset:height-offset,:,:]
    return Image.fromarray(img)

MODEL_PATH = 'D:/DataBaseEffa/MobileNetV2_50epochs2_1ereEtapeEndLayers_60epochs2eme_wholeNetwork2'

IMAGES_00_02 = 'D:/DataBaseEffa/images/videos/newVideos/imagesExtraites/00-02'
IMAGES_02_05 = 'D:/DataBaseEffa/images/videos/newVideos/imagesExtraites/02-05'
IMAGES_05_1 = 'D:/DataBaseEffa/images/videos/newVideos/imagesExtraites/05-1'
IMAGES_1_5 = 'D:/DataBaseEffa/images/videos/newVideos/imagesExtraites/1-5'
IMAGES_5_20 = 'D:/DataBaseEffa/images/videos/newVideos/imagesExtraites/5-20'
IMAGES_20_50 = 'D:/DataBaseEffa/images/videos/newVideos/imagesExtraites/20-50'
IMAGES_50_70 = 'D:/DataBaseEffa/images/videos/newVideos/imagesExtraites/50-70'
IMAGES_70_90 = 'D:/DataBaseEffa/images/videos/newVideos/imagesExtraites/70-90'
IMAGES_90_100 = 'D:/DataBaseEffa/images/videos/newVideos/imagesExtraites/90-100'

TEXT_FILE = 'D:/DataBaseEffa/images/videos/newVideos/videosDone.txt'

VIDEO_PATH = 'D:/DataBaseEffa/images/videos/newVideos/'

model = keras.models.load_model(MODEL_PATH)
print(model.summary())
fileNUM = 0
globalFrame = 0

for file in glob.glob(VIDEO_PATH + "*.mp4"):
    print(os.path.basename(file))
    cap = cv2.VideoCapture(file)
    frameNum=0
    fileNUM+=1
    while(cap.isOpened()):
        ret, frame = cap.read()
        #si la video est finie on passe au fichier suivant
        if ret == 0:
            break
        #on ne traite qu'une image sur 7
        if frameNum == 20:
            frameNum = 0
            cropped = crop_image(frame)
            resized = cv2.resize(np.float32(cropped), (400,400), interpolation = cv2.INTER_AREA)
            normalized = 1./255 * resized 
            transformed = transform.resize(normalized, (400, 400, 3))
            expended = np.expand_dims(transformed, axis=0)
            pred = model.predict(expended, verbose = 0)
            #print(pred[0][0])
            globalFrame += 1
            #cv2.imshow('frame',frame)
            pred[0][0] = pred[0][0] * 100.0
            if pred[0][0] > 0.0 and pred[0][0] <= 0.2:
                #a = a + 1 
                cv2.imwrite(IMAGES_00_02 + '/image' + str(globalFrame) + '.jpg', np.float32(cropped))
                
            if pred[0][0] > 0.2 and pred[0][0] <= 0.5:
                #b = b + 1 
                cv2.imwrite(IMAGES_02_05 + '/image' + str(globalFrame) + '.jpg', np.float32(cropped))
                
            if pred[0][0] > 0.5 and pred[0][0] <= 1.0:
                #c = c + 1 
                cv2.imwrite(IMAGES_05_1 + '/image' + str(globalFrame) + '.jpg', np.float32(cropped))
                
            if pred[0][0] > 1.0 and pred[0][0] <= 5.0:
                #d = d + 1 
                cv2.imwrite(IMAGES_1_5 + '/image' + str(globalFrame) + '.jpg', np.float32(cropped))
                
            if pred[0][0] > 5.0 and pred[0][0] <= 20.0:
                #e = e + 1
                cv2.imwrite(IMAGES_5_20 + '/image' + str(globalFrame) + '.jpg', np.float32(cropped))

            if pred[0][0] > 20.0 and pred[0][0] <= 50.0:
                #b = b + 1 
                cv2.imwrite(IMAGES_20_50 + '/image' + str(globalFrame) + '.jpg', np.float32(cropped))
                
            if pred[0][0] > 50.0 and pred[0][0] <= 70.0:
                #c = c + 1 
                cv2.imwrite(IMAGES_50_70 + '/image' + str(globalFrame) + '.jpg', np.float32(cropped))
                
            if pred[0][0] > 70.0 and pred[0][0] <= 90.0:
                #d = d + 1 
                cv2.imwrite(IMAGES_70_90 + '/image' + str(globalFrame) + '.jpg', np.float32(cropped))
                
            if pred[0][0] > 90.0:
                #e = e + 1
                cv2.imwrite(IMAGES_90_100 + '/image' + str(globalFrame) + '.jpg', np.float32(cropped))

        else:
            frameNum+=1
    print(os.path.basename(file) + ' ' + str(globalFrame) + '\n')
    f = open(TEXT_FILE, "a")
    f.write(os.path.basename(file) + ' ' + str(globalFrame) + '\n' )
    f.close()
    cap.release()
    cv2.destroyAllWindows()
print("nombre de videos traitées : " + str(fileNUM))


