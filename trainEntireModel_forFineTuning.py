import numpy as np
import os
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import backend as K
import matplotlib.pyplot as plt
from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras.layers import Dense,GlobalAveragePooling2D,Dropout
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
import multiprocessing 

train_path = 'D:/DataBaseEffa/images/dataset6_resized400/train/'
valid_path = 'D:/DataBaseEffa/images/dataset6_resized400/valid/'
MODEL_PATH = 'D:/DataBaseEffa/MobileNetV2_1ereEtapeEndLayersImagemodifiees3_/'

K.clear_session()

checkpoint_path = "D:/DataBaseEffa/MobileNetV2_100epochs_1ereEtapeEndLayers_Imagemodifiee3_CP/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)

train_batches = ImageDataGenerator(
                        preprocessing_function=preprocess_input,
                        featurewise_center=True,
                        featurewise_std_normalization=True,
                        channel_shift_range=0.5,
                        zca_whitening=True,
                        brightness_range=[0.85, 1.00],
                        zoom_range=[0.80, 1.00],
                        rotation_range=15,
                        horizontal_flip=True,
                        #fill_mode="wrap",
                        #width_shift_range=0.1
                        #rescale= 1.0/255.0
                        )\
    .flow_from_directory(directory=train_path,
                        target_size=(400, 400),
                        class_mode='binary',
                        batch_size=25,
                        shuffle = True,
                        save_format= 'jpg',
                        classes= ["sansSanglier", "avecSanglier"]
                        )


valid_batches = ImageDataGenerator(
                        preprocessing_function=preprocess_input,
                        #horizontal_flip=True,
                        #rotation_range=20
                        #rescale= 1.0/255.0
                        )\
    .flow_from_directory(directory=valid_path,
                        target_size=(400, 400),
                        class_mode='binary',
                        save_format= 'jpg',
                        classes= ["sansSanglier", "avecSanglier"]
                        )

#load saved model
model = keras.models.load_model(MODEL_PATH)

# Loads the weights of the last check point
model.load_weights(checkpoint_path)

for i,layer in enumerate(model.layers):
  print(i,layer.name,layer.output_shape)
#for layer in model.layers[:-4]:
#  layer.trainable = False

checkpoint_path = "D:/DataBaseEffa/MobileNetV2_100epochs_1ereEtapeEndLayers_Imagemodifiee3_CP/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)
cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                 save_weights_only=True,
                                                 verbose=0)

model.compile(optimizer=Adam(learning_rate=1e-5), loss='binary_crossentropy', metrics=['accuracy'])
es = tf.keras.callbacks.EarlyStopping(monitor='val_binary_accuracy', mode='max', verbose=0, patience=15, restore_best_weights=True)
print(model.summary())

model.fit(
    x=train_batches,
    steps_per_epoch=len(train_batches),
    validation_data=valid_batches,
    validation_steps=len(valid_batches),
    epochs=50,
    verbose=1,
    callbacks=[cp_callback,es]
)

#model.load_weights(checkpoint_path)

model.save('D:/DataBaseEffa/MobileNetV2_1ereEtapeEndLayersImagemodifiees3_WholeModelTrained')



#for i,layer in enumerate(model.layers[:155]):
  #print(i,layer.name,layer.output_shape)
  #layer.trainable = False

#newModel = keras.Sequential()
#for i,layer in enumerate(model.layers):
#  newModel.add(layer)
#print(newModel.summary())
