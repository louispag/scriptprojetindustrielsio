from cv2 import cv2
from tensorflow import keras
import numpy as np
from PIL import Image
from skimage import transform
import glob
import os 

def crop_image(img):
    height, width, ch = img.shape
    if width == height:
        return img
    img = np.array(img)
    offset  = int(abs(height-width)/2)
    if width>height:
        img = img[:,offset:width-offset,:]
    else:
        img = img[offset:height-offset,:,:]
    return Image.fromarray(img)

MODEL_PATH = 'D:/DataBaseEffa/MobileNetV_2emeEtapeNEW_'
VIDEO_PATH = 'D:/DataBaseEffa/images/videos/videos/190 Hogs Down with the Pulsar Thermion XG50.mp4'

model = keras.models.load_model(MODEL_PATH)
print(model.summary())

cap = cv2.VideoCapture(VIDEO_PATH)
fps = cap.get(cv2.CAP_PROP_FPS)
step = fps / 2
print("fps:" + str(fps) + " step: " + str(step))
frameNumber = 0

while(cap.isOpened()):
    ret, frame = cap.read()
    cv2.imshow('frame', frame)
    cv2.waitKey(1)
    if frameNumber == step:
        frameNumber = 1
        cropped = crop_image(frame)
        resized = cv2.resize(np.float32(cropped), (400,400), interpolation = cv2.INTER_AREA)
        transformed = transform.resize(resized, (400, 400, 3))
        expended = np.expand_dims(transformed, axis=0)
        pred = model.predict(expended, verbose = 0)
        print(pred[0][0])
        time.sleep(0.3) #impératif sinon lock
    else:
        frameNumber += 1
        
cap.release()
cv2.destroyAllWindows()
print("VIDEO TERMINEE")