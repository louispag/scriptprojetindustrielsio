from cv2 import cv2
import glob
import numpy as np
import os

path = 'D:/DataBaseEffa/images/dataset6_resized400/trainLight/sansSanglier/'
pathtowrite = 'D:/DataBaseEffa/images/dataset6_resized400/trainLightGray/sansSanglier/'
imgs = []
i = 0
#IMG_SIZE = 700

for file in glob.glob(path + "*"):
    imgs.append(file)
print(len(imgs))
for img in imgs:
    img_array = cv2.imread(img, cv2.IMREAD_GRAYSCALE)
    #print(img_array.shape)
    img2 = np.zeros((400,400,3))
    img2[:,:,0] = img_array
    img2[:,:,1] = img_array
    img2[:,:,2] = img_array
    name = os.path.basename(img)
    #img_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    cv2.imwrite(pathtowrite + name, img2)
    i = i + 1
print(i)

